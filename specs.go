// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import "gitlab.com/entah/fngo"

var (
	// MapType data kind of `map`.
	MapType = WithDesc(
		NewSpec("MapType", fngo.IsMap),
		"Data kind of `map`.",
	)

	// StructType data kind of `struct`.
	StructType = WithDesc(
		NewSpec("StructType", fngo.IsStruct),
		"Data kind of `struct`.",
	)

	// SliceType when data kind of `slice`.
	SliceType = WithDesc(
		NewSpec("SliceType", fngo.IsSlice),
		"Data kind of `slice`.",
	)

	// PointerType data kind of `pointer`.
	PointerType = WithDesc(
		NewSpec("IsPointer", fngo.IsPtr),
		"Data kind of `pointer`",
	)

	// MaybeStructOrMap when data is `map` or `struct`.
	MaybeStructOrMap = WithDesc(
		Or("MaybeStructOrMap", MapType, StructType),
		"Maybe kind of `struct` or `map`.",
	)

	// EmptyData Data either nil or default value.
	EmptyData = WithDesc(
		NewSpec("EmptyData", fngo.IsEmpty),
		"Data is empty or contains default value.",
	)

	// KeyedSpecType ensure `ISpec` is kind of `KeyedSpec` with `PredOpsKeyLookup`.
	KeyedSpecType = WithDesc(
		NewSpec("KeyedSpecType",
			func(spec interface{}) bool {
				return spec.(*Spec).PredOps() == PredOpsKeyLookup
			}),
		"Ensure `ISpec` is kind of `KeyedSpec` with `PredOpsKeyLookup`.",
	)

	// MaybeSliceOfKeyedSpec maybe nil value or slice of `KeyedSpec`.
	MaybeSliceOfKeyedSpec = WithDesc(
		Or("MaybeSliceOfKeyedSpec",
			EmptyData,
			Every("EveryKeyedSpec", KeyedSpecType)),
		"Slice maybe nil value or contains `KeyedSpec`.",
	)

	// TupleSpecType ensure `ISpec` is kind of `KeyedSpec` with `PredOpsTuple`.
	TupleSpecType = WithDesc(
		NewSpec("TupleSpecType",
			func(spec interface{}) bool {
				return spec.(*Spec).PredOps() == PredOpsTuple
			}),
		"Ensure `ISpec` is kind of `KeyedSpec` with `PredOpsTuple`.",
	)

	// FuncType check if data is Function type.
	FuncType = WithDesc(
		NewSpec("IsFunc", fngo.IsFunc),
		"Data is Function type.",
	)
)
