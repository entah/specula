// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import (
	"fmt"
)

// Report contains summary result of validation.
type Report struct {
	FailedSpec      []string
	SpecName        string
	Value           string
	DataPath        string
	Validator       string
	Error           error
	IsPassed        bool
	IsKeyFound      bool
	needErrorDetail bool
}

// // ErrorReport contains error happen or value not passed when validating.
// type ErrorReport struct {
// 	FuncName string
// 	Line     string
// 	Arg      string
// 	Caused   string
// }

// // Error return error string for `gitlab.com/entah/specula#ErrorReport`.
// func (errRep ErrorReport) Error() string {
// 	if errRep.FuncName != "" {
// 		return fmt.Sprintf("%s|func: %s|arg: %s|line:%s", errRep.Caused, errRep.FuncName, errRep.Arg, errRep.Line)
// 	}
// 	return fmt.Sprintf("%s", errRep.Caused)
// }

// ExplainSpec like `gitlab.com/entah/specula#ConformSpec` but return report summary instead of error.
func ExplainSpec(spec ISpec, data interface{}) Report {
	return getReporter(spec)(spec, data, Report{needErrorDetail: true, SpecName: spec.QualifiedName()})
}

func getReporter(spec ISpec) func(spec ISpec, data interface{}, report Report) Report {
	switch spec.PredOps() {
	case PredOpsAnd:
		return andReport
	case PredOpsOr:
		return orReport
	case PredOpsAny:
		return anyReport
	case PredOpsEvery:
		return everyReport
	case PredOpsTuple:
		return tupleReport
	case PredOpsKeyLookup:
		return predOpsKey
	case PredOpsMap:
		return predOpsMap
	case PredOpsDeFunc:
		return defuncReport
	default:
		return report
	}
}

func validate(validator Validator, data interface{}, needErrorDetail bool) (err error) {
	defer func() {
		if r := recover(); r != nil {
			stringArgs := fmt.Sprintf("%v", data)
			err = errorReport(r, validator, stringArgs, needErrorDetail)
		}
	}()
	if validator(data) {
		return nil
	}
	return errorReport("eval: did not pass", validator, fmt.Sprintf("%v", data), needErrorDetail)
}

func report(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	if report.needErrorDetail {
		report.Validator = getFnName(spec.Validator())
	}
	report.SpecName = specName
	if err := validate(spec.Validator(), data, report.needErrorDetail); err != nil {
		report.FailedSpec = append(report.FailedSpec, specName)
		report.Error = err
		report.IsPassed = false
		return report
	}

	report.IsPassed = true
	return report

}

func orReport(spec ISpec, data interface{}, report Report) Report {
	var lastErrorReport Report
	for _, v := range spec.Specs() {
		rep := getReporter(v)(v, data, report)
		if rep.IsPassed {
			return rep
		}

		lastErrorReport = rep
	}
	return lastErrorReport
}

func andReport(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	for _, v := range spec.Specs() {
		report = getReporter(v)(v, data, report)

		if !report.IsPassed {
			report.FailedSpec = append(report.FailedSpec, specName)
			return report
		}
	}
	return report
}
