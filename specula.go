// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

// Package specula Specula[tives] of data in golang.
package specula

import (
	"fmt"
	"strings"
	"sync"
)

// registries contains all qualified name spec to avoid clashing with each other.
type registries struct {
	index map[string]string
	lock  sync.Mutex
}

// specRegistries hold qualified name of created spec.
var specRegistries = registries{index: map[string]string{}}

// Validator func to test value, must in format `func(interface{}) bool`.
type Validator func(interface{}) bool

const (
	// PredValidator spec that hold validator.
	PredValidator = "FUNC"
	// PredOpsOr OR logic for a set of predictions.
	PredOpsOr = "OR"
	// PredOpsAnd AND logic for a set of predictions.
	PredOpsAnd = "AND"
	// PredOpsAny short circuit for the first passed value, only work for slice or array.
	PredOpsAny = "ANY"
	// PredOpsEvery short circuit for the first not passed value, only work for slice or array.
	PredOpsEvery = "EVERY"
	// PredOpsTuple predicate for a fixed slice or array.
	PredOpsTuple = "TUPLE"
	// PredOpsKeyLookup check if value under key is passed, only work for map or struct.
	PredOpsKeyLookup = "KEY"
	// PredOpsMap check if map or struct is passed.
	PredOpsMap = "MAP"
	// PredOpsDeFunc for function definition/signature.
	PredOpsDeFunc = "DEFUNC"
)

// Spec contains specification for value being checked.
type Spec struct {
	specs         []ISpec
	opts          []ISpec
	reqs          []ISpec
	validator     Validator
	name          string
	qualifiedName string
	predOps       string
	namespace     string
	description   string
	// structTag   string
}

// ISpec interface to perform validation.
type ISpec interface {
	Name() string
	QualifiedName() string
	Validator() Validator
	PredOps() string
	Specs() []ISpec
	Opts() []ISpec
	Req() []ISpec
	// StructTag() string
}

// Name get `gitlab.com/entah/specula/#Spec` name.
func (spc *Spec) Name() string {
	return spc.name
}

// QualifiedName get `gitlab.com/entah/specula/#Spec` qualified name contains pkg path and the name of spec.
func (spc Spec) QualifiedName() string {
	return spc.qualifiedName
}

// Validator get `gitlab.com/entah/specula/#Spec` function to use as validator.
func (spc Spec) Validator() Validator {
	return spc.validator
}

// PredOps get `gitlab.com/entah/specula/#Spec` function to use as validator.
func (spc Spec) PredOps() string {
	return spc.predOps
}

// Specs get `gitlab.com/entah/specula/#Spec` child specs.
func (spc Spec) Specs() []ISpec {
	return spc.specs
}

// // StructTag get `gitlab.com/entah/specula/#Spec` struct tag to search for struct key.
// func (spc Spec) StructTag() string {
// 	return spc.structTag
// }

// Opts get `gitlab.com/entah/specula/#Spec` optionals keyed spec.
func (spc Spec) Opts() []ISpec {
	return spc.opts
}

// Req get `gitlab.com/entah/specula/#Spec` required keyed spec.
func (spc Spec) Req() []ISpec {
	return spc.reqs
}

// NewSpec create a new spec using `gitlab.com/entah/specula/#validator`.
func NewSpec(name string, validator Validator) ISpec {
	joiner := ":F:"
	namespace := getNamespaceCaller()
	qualifiedName := namespace + joiner + name
	insertOrPanic(qualifiedName)

	return &Spec{
		name:          name,
		qualifiedName: qualifiedName,
		namespace:     namespace,
		validator:     validator,
		predOps:       PredValidator,
	}
}

// PredFrom create a new spec using some `specs` for specific operations `ops`.
func PredFrom(ops, namespace, name string, specs ...ISpec) ISpec {
	joiner := ":::"
	qualifiedName := namespace + joiner + name
	insertOrPanic(qualifiedName)
	return &Spec{
		name:          name,
		qualifiedName: qualifiedName,
		namespace:     namespace,
		predOps:       strings.ToUpper(ops),
		specs:         specs,
	}
}

// And create a new spec with AND logic.
func And(name string, specs ...ISpec) ISpec {
	return PredFrom(PredOpsAnd, getNamespaceCaller(), name, specs...)
}

// Or create a new spec with OR logic.
func Or(name string, specs ...ISpec) ISpec {
	return PredFrom(PredOpsOr, getNamespaceCaller(), name, specs...)
}

// Any create a new spec for used by slice or array, will be short-circuited on the first passed value.
func Any(name string, specs ...ISpec) ISpec {
	return PredFrom(PredOpsAny, getNamespaceCaller(), name, specs...)
}

// Every create a new spec for used by slice or array, will be short-circuited on the first not passed value.
func Every(name string, specs ...ISpec) ISpec {
	return PredFrom(PredOpsEvery, getNamespaceCaller(), name, specs...)
}

// Tuple create a new spec for used by fixed slice or array.
func Tuple(name string, specs ...ISpec) ISpec {
	return PredFrom(PredOpsTuple, getNamespaceCaller(), name, specs...)
}

// DeFunc create a new spec for function definition/signature,
// `input` and `output` must be Tuple Spec and your validator must accept `reflect.Type`.
func DeFunc(name string, input ISpec, output ISpec) ISpec {
	panicOnError(ConformSpec(TupleSpecType, input))
	panicOnError(ConformSpec(TupleSpecType, output))
	return PredFrom(PredOpsDeFunc, getNamespaceCaller(), name, input, output)
}

// KeyedSpec create a new keyed spec for used by struct or map.
func KeyedSpec(key string, specs ...ISpec) ISpec {
	name := key
	namespace := getNamespaceCaller()
	joiner := ":K:"
	qualifiedName := namespace + joiner + name
	insertOrPanic(qualifiedName)
	return &Spec{
		name:          name,
		qualifiedName: qualifiedName,
		predOps:       PredOpsKeyLookup,
		specs:         specs,
		namespace:     namespace,
	}
}

// NewSpecMap create a new spec for used by struct or map, `req` and `opts` must be keyed spec,
// will be short circuited if one of `reqs` spec not found or not passed,
// `opts` keyed spec only validated if found,
// will panic if `reqs` or `opts` is not keyed spec.
func NewSpecMap(name string, reqs []ISpec, opts []ISpec) ISpec {
	panicOnError(ConformSpec(MaybeSliceOfKeyedSpec, reqs))
	panicOnError(ConformSpec(MaybeSliceOfKeyedSpec, opts))

	namespace := getNamespaceCaller()
	joiner := ":M:"

	qualifiedName := namespace + joiner + name
	insertOrPanic(qualifiedName)

	return &Spec{
		name:          name,
		qualifiedName: namespace + joiner + name,
		namespace:     namespace,
		predOps:       PredOpsMap,
		reqs:          reqs,
		opts:          opts,
	}
}

// template for error generated by `gitlab.com/entah/specula#ConformSpec`
const (
	lowLvlSpecErrFmt      = "%+v not conform %s caused by %v"
	highLvlSpecErrFmt     = "%+v not conform %s, %v, via %s"
	highLvlSpecDataErrFmt = "%+v not conform %s, %v in %s, via %s"
	keySpecNotFoundFmt    = "key %v not found in %+v to satisfy %s"
)

// ConformSpec check if `data` passed.
func ConformSpec(spec ISpec, data interface{}) error {
	report := getReporter(spec)(spec, data, Report{needErrorDetail: false})
	specName := spec.QualifiedName()
	name := spec.Name()
	switch {

	case spec.PredOps() == PredValidator && !report.IsPassed:
		return fmt.Errorf(lowLvlSpecErrFmt, data, report.Error, report.FailedSpec[0])

	case spec.PredOps() == PredOpsKeyLookup && !report.IsPassed && !report.IsKeyFound:
		return fmt.Errorf(keySpecNotFoundFmt, name, data, specName)

	case !report.IsPassed:
		specName := spec.QualifiedName()
		if report.DataPath != "" {
			return fmt.Errorf(highLvlSpecDataErrFmt, data, specName, report.Error, report.DataPath, strings.Join(report.FailedSpec, " <= "))
		}
		return fmt.Errorf(highLvlSpecErrFmt, data, specName, report.Error, strings.Join(report.FailedSpec, " <= "))

	default:
		return nil
	}
}

// PassedBy return true if called `gitlab.com/entah/specula#ConformSpec` with no error.
func PassedBy(spec ISpec, data interface{}) bool {
	report := getReporter(spec)(spec, data, Report{needErrorDetail: false})
	return report.IsPassed
}

// WithDesc add description to a spec.
func WithDesc(spec ISpec, desc string) ISpec {
	if s, ok := spec.(*Spec); ok {
		s.description = desc
		return s
	}
	return spec
}
