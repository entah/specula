package specula

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrinting(t *testing.T) {
	desc1 := DescribeSpec(EmptyData)
	jsonStr := JSONString(desc1)
	assert.Equal(t, "{\"SpecQualifiedName\":\"gitlab.com/entah/specula:F:EmptyData\",\"PredOperation\":\"FUNC\",\"Validators\":\"fngo.IsEmpty gitlab.com/entah/fngo/type_check.go:79\",\"Description\":\"Data is empty or contains default value.\"}", jsonStr)
	jsonStr2 := JSONIdentString(desc1)
	assert.Equal(t, "{\n  \"SpecQualifiedName\": \"gitlab.com/entah/specula:F:EmptyData\",\n  \"PredOperation\": \"FUNC\",\n  \"Validators\": \"fngo.IsEmpty gitlab.com/entah/fngo/type_check.go:79\",\n  \"Description\": \"Data is empty or contains default value.\"\n}", jsonStr2)

	assert.Panics(t, func() { JSONString(math.NaN()) })
	assert.Panics(t, func() { JSONIdentString(math.NaN()) })
}
