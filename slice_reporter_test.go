package specula

import (
	"reflect"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/entah/fngo"
)

func Test_everyReport(t *testing.T) {
	isOdd := func(i interface{}) bool {
		return i.(int)%2 != 0
	}

	mustOdd := NewSpec("mustOdd", isOdd)
	everyOdd := Every("EveryOdd", mustOdd)

	assert.NoError(t, ConformSpec(everyOdd, []int{1, 3, 5, 9}))
	assert.Error(t, ConformSpec(everyOdd, []int{2, 4, 6, 10}))
	assert.Error(t, ConformSpec(everyOdd, 1))
}

func Test_anyReport(t *testing.T) {
	lessThanTen := func(i interface{}) bool {
		return i.(int) < 10
	}

	greaterThanZero := func(i interface{}) bool {
		return i.(int) > 0
	}

	rangeOfTen := WithDesc(
		Any(
			"RangeOfTen",
			NewSpec("LessThanTen", lessThanTen),
			NewSpec("GreaterThanZero", greaterThanZero),
		),
		" 0 < number < 10",
	)

	assert.NoError(t, ConformSpec(rangeOfTen, []int{1, 3, 5, 9}))
	assert.NoError(t, ConformSpec(rangeOfTen, []int{2, 4, 6, 10}))
	assert.Error(t, ConformSpec(rangeOfTen, 1))
}

func Test_tupleReport(t *testing.T) {
	isInt := func(i interface{}) bool {
		return reflect.TypeOf(i).Kind() == reflect.Int
	}

	greaterThanMinus := func(i interface{}) bool {
		return i.(int) > -1
	}

	steps := Or(
		"MaybeInt",
		NewSpec("isEmpty", fngo.IsEmpty),
		NewSpec("isInt", isInt),
	)

	startStep := And(
		"GreaterThanMinus",
		NewSpec("isInt2", isInt),
		NewSpec("GreaterThanMinus", greaterThanMinus),
	)

	rangeArgs := Tuple("RangeArgs", steps, startStep)

	assert.NoError(t, ConformSpec(rangeArgs, []interface{}{nil, 0}))
	assert.NoError(t, ConformSpec(rangeArgs, []interface{}{0, 1}))
	assert.Error(t, ConformSpec(rangeArgs, []interface{}{0, "1"}))
	assert.Error(t, ConformSpec(rangeArgs, []int{1, 2, 3}))
	assert.Error(t, ConformSpec(rangeArgs, nil))
}

func Test_defuncReport(t *testing.T) {
	isStringType := func(i interface{}) bool {
		arg := i.(reflect.Type)
		return arg.Kind() == reflect.String
	}

	var sideEffect interface{} = func() {}

	sideEffectFunc := DeFunc("SideEffectFunc", Tuple("NoArgs", nil), Tuple("NoReturn", nil))
	stringOpsFunc := DeFunc(
		"StringArgReturnString",
		Tuple("StringArg", NewSpec("IsStringType", isStringType)),
		Tuple("StringRet", NewSpec("IsStringType2", isStringType)),
	)

	greetFunc := func(name string) string {
		return "hello " + name
	}

	assert.NoError(t, ConformSpec(stringOpsFunc, greetFunc))
	assert.NoError(t, ConformSpec(sideEffectFunc, sideEffect))
	assert.Error(t, ConformSpec(stringOpsFunc, sideEffect))
	assert.Error(t, ConformSpec(stringOpsFunc, strconv.Itoa))
	assert.Error(t, ConformSpec(stringOpsFunc, func(s string) int {
		p, e := strconv.Atoi(s)
		if e != nil {
			return 0
		}
		return p
	}))
	assert.Error(t, ConformSpec(stringOpsFunc, nil))

}
