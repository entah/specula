// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import (
	"fmt"
	"reflect"

	"gitlab.com/entah/fngo"
)

const slicePathFormat = "%s.[%v]"

func anyReport(spec ISpec, data interface{}, report Report) Report {
	if rep := ensureSliceNotEmpty(spec, data, report); rep.Error != nil {
		return rep
	}

	rfSlice := ensureNotPtr(data)
	lenData := rfSlice.Len()
	dataPath := report.DataPath
	lastErrorReport := report
	for i := 0; i < lenData; i++ {
		report.DataPath = fmt.Sprintf(slicePathFormat, dataPath, i)
		for _, v := range spec.Specs() {
			val := rfSlice.Index(i).Interface()
			rep := getReporter(v)(v, val, report)

			if rep.IsPassed {
				return rep
			}

			lastErrorReport = rep
		}
	}
	return lastErrorReport
}

func everyReport(spec ISpec, data interface{}, report Report) Report {
	if rep := ensureSliceNotEmpty(spec, data, report); rep.Error != nil {
		return rep
	}

	specName := spec.QualifiedName()
	rfSlice := ensureNotPtr(data)
	lenData := rfSlice.Len()
	dataPath := report.DataPath
	for i := 0; i < lenData; i++ {
		report.DataPath = fmt.Sprintf(slicePathFormat, dataPath, i)
		for _, v := range spec.Specs() {
			val := rfSlice.Index(i).Interface()
			report = getReporter(v)(v, val, report)

			if !report.IsPassed {
				report.FailedSpec = append(report.FailedSpec, specName)
				report.IsPassed = false
				return report
			}
		}
	}
	report.IsPassed = true
	return report
}

func tupleReport(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	if !fngo.IsSlice(data) {
		return createErrorReport(report, spec, data, "not slice or array")
	}

	rfSlice := ensureNotPtr(data)
	lenData := rfSlice.Len()
	lenSpec := countSpecs(spec.Specs())

	if lenData != lenSpec {
		return createErrorReport(report, spec, data, "tuple: slice len must match total spec")
	}

	dataPath := report.DataPath
	report.IsPassed = true
	for i := 0; i < lenData; i++ {
		report.DataPath = fmt.Sprintf(slicePathFormat, dataPath, i)

		val := rfSlice.Index(i).Interface()
		v := spec.Specs()[i]
		report = getReporter(v)(v, val, report)

		if !report.IsPassed {
			report.FailedSpec = append(report.FailedSpec, specName)
			return report
		}
	}
	return report
}

func defuncReport(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	rt := reflect.TypeOf(ensureValue(data).Interface())

	if rt.Kind() != reflect.Func {
		return createErrorReport(report, spec, data, "not function type")
	}

	inLen := rt.NumIn()
	outLen := rt.NumOut()
	specIn := spec.Specs()[0]
	specOut := spec.Specs()[1]

	inTypes := make([]reflect.Type, inLen)
	for i := 0; i < inLen; i++ {
		inTypes[i] = rt.In(i)
	}

	report = getReporter(specIn)(specIn, inTypes, report)

	if !report.IsPassed {
		report.FailedSpec = append(report.FailedSpec, specName)
		return report
	}

	outTypes := make([]reflect.Type, outLen)
	for i := 0; i < outLen; i++ {
		outTypes[i] = rt.Out(i)
	}

	report = getReporter(specOut)(specOut, outTypes, report)

	if !report.IsPassed {
		report.FailedSpec = append(report.FailedSpec, specName)
		return report
	}

	return report
}
