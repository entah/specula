// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import (
	"reflect"

	"gitlab.com/entah/fngo"
)

func predOpsKey(spec ISpec, data interface{}, report Report) Report {
	switch {
	case fngo.IsMap(data):
		return mapKeyReport(spec, data, report)
	case fngo.IsStruct(data):
		return structKeyReport(spec, data, report)
	default:
		return createErrorReport(report, spec, data, "not struct or map")
	}
}

func structKeyReport(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	rv := ensureNotPtr(data)
	val := rv.FieldByName(spec.Name())
	report.DataPath = "." + spec.Name()
	if val.IsValid() {
		report.IsKeyFound = true
		for _, v := range spec.Specs() {
			valIface := val.Interface()
			report = getReporter(v)(v, valIface, report)

			if !report.IsPassed {
				report.FailedSpec = append(report.FailedSpec, specName)
				return report
			}
		}
		return report
	}
	// report.Value = data
	report.FailedSpec = append(report.FailedSpec, specName)
	return report
}

func mapKeyReport(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	rv := ensureNotPtr(data)
	key := reflect.ValueOf(spec.Name())
	report.DataPath = "." + spec.Name()
	if val := rv.MapIndex(key); val.IsValid() {
		report.IsKeyFound = true
		for _, v := range spec.Specs() {
			valIface := val.Interface()
			report = getReporter(v)(v, valIface, report)

			if !report.IsPassed {
				report.FailedSpec = append(report.FailedSpec, specName)
				return report
			}
		}
		return report
	}
	// report.Value = data
	report.FailedSpec = append(report.FailedSpec, specName)
	return report
}

func predOpsMap(spec ISpec, data interface{}, report Report) Report {
	specName := spec.QualifiedName()
	if !fngo.IsStruct(data) && !fngo.IsMap(data) {
		return createErrorReport(report, spec, data, "not struct or map")
	}

	for _, req := range spec.Req() {
		report = getReporter(req)(req, data, report)

		if !report.IsPassed {
			report.FailedSpec = append(report.FailedSpec, specName)
			return report
		}
	}

	for _, opt := range spec.Opts() {
		report = getReporter(opt)(opt, data, report)
		if report.IsKeyFound && !report.IsPassed {
			report.FailedSpec = append(report.FailedSpec, specName)
			return report
		}
	}

	return report
}
