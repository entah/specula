// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

// DescribedSpec contains summary of spec.
type DescribedSpec struct {
	SpecQualifiedName string
	PredOperation     string          `json:",omitempty"`
	IsKeySpec         bool            `json:",omitempty"`
	Validators        string          `json:",omitempty"`
	Description       string          `json:",omitempty"`
	RequiredKeys      []string        `json:",omitempty"`
	OptionalKeys      []string        `json:",omitempty"`
	AppliedSpec       []DescribedSpec `json:",omitempty"`
}

// DescribeSpec summarize `spec`.
func DescribeSpec(spec ISpec) DescribedSpec {
	described := DescribedSpec{
		SpecQualifiedName: spec.QualifiedName(),
		IsKeySpec:         spec.PredOps() == PredOpsKeyLookup,
		PredOperation:     spec.PredOps(),
		Description:       getDesc(spec),
	}

	if spec.Validator() != nil {
		described.Validators = getFnName(spec.Validator())
	}

	childsSpec := spec.Specs()

	if len(childsSpec) > 0 {
		childsMap := []DescribedSpec{}
		for _, child := range childsSpec {
			childsMap = append(childsMap, DescribeSpec(child))
		}
		described.AppliedSpec = childsMap
	}

	reqs := spec.Req()

	if len(reqs) > 0 {
		childsMap := []DescribedSpec{}
		names := []string{}
		for _, child := range reqs {
			childsMap = append(childsMap, DescribeSpec(child))
			names = append(names, child.QualifiedName())
		}
		described.AppliedSpec = append(described.AppliedSpec, childsMap...)
		described.RequiredKeys = names
	}

	opts := spec.Opts()

	if len(opts) > 0 {
		childsMap := []DescribedSpec{}
		names := []string{}
		for _, child := range opts {
			childsMap = append(childsMap, DescribeSpec(child))
			names = append(names, child.QualifiedName())
		}
		described.AppliedSpec = append(described.AppliedSpec, childsMap...)
		described.OptionalKeys = names
	}

	return described
}
