package specula

import (
	"reflect"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Address struct {
	City     string
	Province string
}

type Person struct {
	Name    string
	Age     int
	Friends []Person
	Address Address
	Hobbies []string
	Todos   interface{}
}

func isValidAge(age interface{}) bool {
	return age.(int) > 0 && age.(int) < 100
}

func isValidName(name interface{}) bool {
	return regexp.MustCompile(`^([A-Za-z\s]+)$`).MatchString(name.(string))
}

func isValidHobby(hobby interface{}) bool {
	return regexp.MustCompile(`^(Music|Coding|Walking|Sleeping)$`).MatchString(hobby.(string))
}

var (
	aa = Person{Name: "aa"}
	ab = Person{Name: "ab"}
	ac = Person{Name: "ac"}
	ad = Person{Name: "ad"}
	ae = Person{Name: "ad"}

	validAge = WithDesc(
		NewSpec("validAge", isValidAge),
		"valid age is > 0 and < 100 years",
	)
	validHobby = WithDesc(
		NewSpec("validHobby", isValidHobby),
		"hobby must one of Music, Coding, Walking, Sleeping",
	)

	validName = WithDesc(
		NewSpec("validName", isValidName),
		"user name must comply ^([A-Za-z\\s]+)$ this regex",
	)

	maybeCity  = Or("maybeCity", EmptyData, validName)
	maybeTodos = Or("maybeTodos", EmptyData, MapType)
	everyHobby = Every("everyHobby", validHobby)

	keyName     = KeyedSpec("Name", validName)
	keyAge      = KeyedSpec("Age", validAge)
	keyCity     = KeyedSpec("City", validName)
	keyProvince = KeyedSpec("Province", maybeCity)

	keyAddress = KeyedSpec("Address", addressSpec)
	keyHobby   = KeyedSpec("Hobbies", everyHobby)
	keyTodos   = KeyedSpec("Todos", maybeTodos)

	addressSpec = NewSpecMap("Address", []ISpec{keyCity, keyProvince}, nil)

	personSpec = WithDesc(
		NewSpecMap("Person",
			[]ISpec{
				keyName,
				KeyedSpec("Friends",
					Every("FriendPerson",
						NewSpecMap("Friends",
							[]ISpec{
								keyName,
							},
							nil),
					),
				),
			},
			[]ISpec{keyTodos},
		),
		"Spec for validating Person struct under gitlab.com/entah/specula",
	)
)

func TestTreeStructure(t *testing.T) {
	aa.Friends = []Person{ab, ac, ad, ae}

	ma := map[string]interface{}{
		"Name": "Foo",
		"Friends": []interface{}{
			map[string]interface{}{"Name": "bar"},
			map[string]interface{}{"Name": "baz"},
		},
	}

	a := Person{
		Name:    "foobar",
		Age:     16,
		Hobbies: []string{"Music", "Swimming"},
	}

	assert.NoError(t, ConformSpec(personSpec, aa))
	assert.NoError(t, ConformSpec(personSpec, ma))

	personReport := ExplainSpec(NewSpecMap(
		"NewPerson",
		[]ISpec{keyName, keyAge, keyAddress, keyHobby},
		[]ISpec{keyTodos},
	), a)

	assert.NotEmpty(t, personReport)
	assert.NotEmpty(t, DescribeSpec(EmptyData))
	assert.NotEmpty(t, DescribeSpec(keyAddress))
	assert.NotEmpty(t, DescribeSpec(personSpec))

}

func userNameMatcher(name interface{}) bool {
	return regexp.MustCompile(`[a-zA-Z]+`).MatchString(name.(string))
}

func isString(val interface{}) bool {
	if val == nil {
		return false
	}
	ifaceVal := reflect.ValueOf(val).Interface()
	rf := reflect.ValueOf(ifaceVal)
	return reflect.Indirect(rf).Type().Kind() == reflect.String
}

func TestConformSpec(t *testing.T) {
	isUserName := NewSpec("isUserName", userNameMatcher)
	stringType := NewSpec("isString", isString)

	userName := And("UserName", stringType, isUserName)
	maybeUsername := Or("MaybeUserName", stringType, isUserName)
	anyOfUsername := Any("AnyOfUserName", userName)
	everyUsername := Every("EveryUserName", userName)
	everyString := Every("EveryString", stringType)
	anyString := Any("AnyString", stringType)

	assert.Error(t, ConformSpec(isUserName, 1234))
	assert.Error(t, ConformSpec(isUserName, "1234"))
	assert.NoError(t, ConformSpec(isUserName, "foobar"))
	assert.NoError(t, ConformSpec(MaybeSliceOfKeyedSpec, []ISpec{keyAge, keyCity}))
	assert.Error(t, ConformSpec(everyString, []string{}))

	assert.Error(t, ConformSpec(userName, 1234))
	assert.Error(t, ConformSpec(maybeUsername, 1234))
	assert.NoError(t, ConformSpec(maybeUsername, "1234"))
	assert.NoError(t, ConformSpec(maybeUsername, "foobar"))
	assert.NoError(t, ConformSpec(userName, "Foobar"))
	assert.NoError(t, ConformSpec(anyOfUsername, []interface{}{"foobar", 1234}))
	assert.Error(t, ConformSpec(anyOfUsername, []interface{}{4321, 1234}))
	assert.Error(t, ConformSpec(everyUsername, []interface{}{"foobar", 1234}))
	assert.Error(t, ConformSpec(everyString, []interface{}{"foobar", 1234}))
	assert.NoError(t, ConformSpec(anyString, []interface{}{"foobar", 1234}))

	keyMapName := KeyedSpec("name", isUserName)

	assert.Error(t, ConformSpec(keyMapName, map[string]string{"name": "123"}))
	assert.Error(t, ConformSpec(keyMapName, map[string]interface{}{"name": 123}))
	assert.NoError(t, ConformSpec(keyMapName, map[string]string{"name": "foobar"}))

	keyMapName2 := KeyedSpec("nickName", maybeUsername, userName)
	assert.Panics(t, func() {
		KeyedSpec("nickName", maybeUsername, userName)
		KeyedSpec("nickName", maybeUsername, userName)
	})
	assert.Error(t, ConformSpec(keyMapName2, map[string]string{"name": "foobar"}))
	assert.Error(t, ConformSpec(keyMapName2, map[string]interface{}{"nickName": 123}))
	assert.Error(t, ConformSpec(keyMapName2, map[string]interface{}{"nickName": "123"}))
	assert.NotEmpty(t, ExplainSpec(keyMapName2, map[string]interface{}{"nickName": "123"}).FailedSpec)
	assert.NoError(t, ConformSpec(keyMapName2, map[string]interface{}{"nickName": "foobar"}))
	assert.True(t, PassedBy(keyMapName2, map[string]interface{}{"nickName": "foobar"}))
}

type anotherSpec struct{}

func (as anotherSpec) Name() string {
	return ""
}

func (as anotherSpec) QualifiedName() string {
	return ""
}

func (as anotherSpec) Validator() Validator {
	return nil
}

func (as anotherSpec) PredOps() string {
	return ""
}

func (as anotherSpec) Specs() []ISpec {
	return []ISpec{}
}

func (as anotherSpec) Opts() []ISpec {
	return []ISpec{}
}

func (as anotherSpec) Req() []ISpec {
	return []ISpec{}
}

func TestWithDesc(t *testing.T) {
	anoSpec := &anotherSpec{}
	withDesc := WithDesc(anoSpec, "This desc will not attached to `ISpec`.")
	assert.Empty(t, getDesc(withDesc))
	// assert.Equal(t, 0, getChildNode(anoSpec))

	spc := NewSpec("dummSpec", nil)
	spcWithDesc := WithDesc(spc, "This desc will attached to `ISpec`.")
	assert.Equal(t, "This desc will attached to `ISpec`.", getDesc(spcWithDesc))

}
