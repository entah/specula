package specula

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_structKeyReport(t *testing.T) {
	type p struct {
		Name string
		Age  int
	}

	type P struct {
		FirstName string
	}

	assert.NoError(t, ConformSpec(keyName, p{Name: "Foobar"}))
	assert.NoError(t, ConformSpec(keyName, &p{Name: "Foobar"}))
	assert.Error(t, ConformSpec(keyName, &p{Name: "Foobar123"}))
	assert.Error(t, ConformSpec(keyName, nil))
	assert.Error(t, ConformSpec(keyName, P{"Foo"}))
	assert.Error(t, ConformSpec(keyName, &P{"Foo"}))

	pSpec := NewSpecMap("pSpec", []ISpec{keyName}, []ISpec{keyAge})
	assert.NoError(t, ConformSpec(pSpec, p{Name: "Foobar", Age: 18}))
	assert.NoError(t, ConformSpec(pSpec, &p{Name: "Foobar", Age: 19}))
	assert.Error(t, ConformSpec(pSpec, &p{Name: "Foobar"}))
	assert.Error(t, ConformSpec(pSpec, &p{Name: "Foobar123", Age: 20}))
	assert.Error(t, ConformSpec(pSpec, nil))
	assert.Error(t, ConformSpec(pSpec, P{"Foo"}))
	assert.Error(t, ConformSpec(pSpec, &P{"Foo"}))

	assert.Panics(t, func() {
		NewSpecMap("SpecMustPanic", keyName.Specs(), nil)
	})

	assert.Panics(t, func() {
		NewSpecMap("SpecMustPanic", []ISpec{keyName}, keyAge.Specs())
	})
}

func Test_mapKeyReport(t *testing.T) {
	assert.NoError(t, ConformSpec(keyName, map[string]string{"Name": "Foobar"}))
	assert.NoError(t, ConformSpec(keyName, &map[string]string{"Name": "Foobar"}))
	assert.NoError(t, ConformSpec(keyName, &map[string]interface{}{"Name": "Foobar"}))
	assert.NoError(t, ConformSpec(keyName, &map[interface{}]string{"Name": "Foobar"}))
	assert.NoError(t, ConformSpec(keyName, &map[interface{}]interface{}{"Name": "Foobar"}))
	assert.Error(t, ConformSpec(keyName, &map[string]string{"Name": "Foobar123"}))
	assert.Error(t, ConformSpec(keyName, nil))
}
