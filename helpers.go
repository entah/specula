// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import (
	"fmt"
	"path"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/entah/fngo"
)

// FuncInfo contains info of func.
type FuncInfo struct {
	FuncName string
	PkgPath  string
	FileName string
	Line     string
}

// GetFuncInfo return info of func.
func GetFuncInfo(fn interface{}) FuncInfo {
	rfn := runtime.FuncForPC(reflect.ValueOf(fn).Pointer())
	splittedFnName := strings.Split(rfn.Name(), "/")
	fnName := splittedFnName[len(splittedFnName)-1]
	pkgPath := strings.Join(append(splittedFnName[0:len(splittedFnName)-1], strings.Split(fnName, ".")[0]), "/")
	pathFn, line := rfn.FileLine(rfn.Entry())
	_, file := path.Split(pathFn)
	return FuncInfo{
		FuncName: fnName,
		PkgPath:  pkgPath,
		FileName: file,
		Line:     strconv.Itoa(line),
	}
}

func errorReport(cause, fn interface{}, arg string, needErrorDetail bool) error {
	stringCause := fmt.Sprintf("%s", cause)

	if spec, ok := fn.(ISpec); ok {
		return fmt.Errorf("%s => (%s) => %s", spec.QualifiedName(), arg, stringCause)
	}

	if !needErrorDetail {
		return fmt.Errorf("(%s) => %s", arg, stringCause)
	}

	funcInfo := GetFuncInfo(fn)
	return fmt.Errorf("%s:%s(%s) => %s", stringCause, funcInfo.FuncName, funcInfo.Line, arg)
}

func getFnName(fn interface{}) string {
	funcInfo := GetFuncInfo(fn)
	return fmt.Sprintf("%s %s:%v", funcInfo.FuncName, funcInfo.PkgPath+"/"+funcInfo.FileName, funcInfo.Line)
}

func getDesc(spec ISpec) string {
	if s, ok := spec.(*Spec); ok {
		return s.description
	}
	return ""
}

func ensureValue(data interface{}) reflect.Value {
	if data == nil {
		return reflect.ValueOf((*Spec)(nil))
	}
	return reflect.ValueOf(data)
}

func ensureNotPtr(data interface{}) reflect.Value {
	if fngo.IsPtr(data) {
		return reflect.Indirect(reflect.ValueOf(data))
	}
	return ensureValue(data)
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func countSpecs(specs []ISpec) int {
	if len(specs) == 1 {
		if specs[0] == nil {
			return 0
		}
	}
	return len(specs)
}

func createErrorReport(report Report, spec ISpec, data interface{}, errString string) Report {
	stringArg := fmt.Sprintf("%v", data)
	specName := spec.QualifiedName()
	report.Error = errorReport(errString, spec, stringArg, report.needErrorDetail)
	report.SpecName = specName
	report.Value = stringArg
	report.FailedSpec = append(report.FailedSpec, specName)
	report.IsPassed = false
	return report
}

func ensureSliceNotEmpty(spec ISpec, data interface{}, report Report) Report {
	if !fngo.IsSlice(data) {
		return createErrorReport(report, spec, data, "not slice or array")
	}

	if fngo.IsEmpty(data) {
		return createErrorReport(report, spec, data, "cannot work with empty slice or array")
	}

	return report
}

func getNamespaceCaller() string {
	pc := make([]uintptr, 4)
	n := runtime.Callers(0, pc)

	frame := runtime.Frame{Function: "namespace"}
	if n > 0 {
		frames := runtime.CallersFrames(pc)
		for {
			maybeTarget, more := frames.Next()
			if !more || strings.Contains(maybeTarget.Function, ".init") {
				frame = maybeTarget
				break
			}
		}
	}
	splitted := strings.Split(frame.Function, ".")
	return strings.Join(splitted[:len(splitted)-1], ".")
}

func insertOrPanic(qualifiedName string) {
	declaredLoc := getCallerLine()

	specRegistries.lock.Lock()
	defer specRegistries.lock.Unlock()

	if loc, found := specRegistries.index[qualifiedName]; found {
		panic(fmt.Sprintf("%s has declared in %s redeclared in %s", qualifiedName, loc, declaredLoc))
	}

	specRegistries.index[qualifiedName] = declaredLoc
}

func getCallerLine() string {
	_, file, line, _ := runtime.Caller(3)
	return file + ":" + strconv.Itoa(line)
}
