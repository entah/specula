// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package specula

import "encoding/json"

// JSONString return JSON string representation of `i`.
func JSONString(i interface{}) string {
	arrBytes, err := json.Marshal(&i)
	if err != nil {
		panic(err)
	}
	return string(arrBytes)
}

// JSONIdentString return JSON indented string representation of `i`.
func JSONIdentString(i interface{}) string {
	arrBytes, err := json.MarshalIndent(&i, "", "  ")
	if err != nil {
		panic(err)
	}
	return string(arrBytes)
}
